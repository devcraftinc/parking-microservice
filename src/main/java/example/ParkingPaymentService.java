package example;

public class ParkingPaymentService {

	public static interface Clock{
		long now();
	}

	private Clock clock;
	private PricingPolicy pricingPolicy = new PricingPolicy();

	private ParkingStore store;
	
	public ParkingPaymentService(Clock clock, ParkingStore parkingStore) {
		this.clock = clock;
		store= parkingStore;
	}
	
	public long enterParking() {
		return store.storeParkingEntry(clock.now());
	}

	public long calcPayment(long codeOnCard) {
		long paymentTime = clock.now();
		long entryTime = store.fetchEntryTime(codeOnCard);
		return pricingPolicy.calcPrice(entryTime, paymentTime);
	}
}