package example;

class JpaParkingStore implements ParkingStore {
	
	protected ParkingRepository repository;

	public JpaParkingStore(ParkingRepository repository) {
		this.repository = repository;
	}

	/* (non-Javadoc)
	 * @see example.ParkingStore#fetchEntryTime(long)
	 */
	@Override
	public long fetchEntryTime(long codeOnCard) {
		return repository.findById(codeOnCard).get().time;
	}
	
	/* (non-Javadoc)
	 * @see example.ParkingStore#storeParkingEntry(long)
	 */
	@Override
	public long storeParkingEntry(long time) {
		ParkingRepository.Parking p = new ParkingRepository.Parking();
		p.time =time;
		p = repository.save(p);
		long code = p.code;
		return code;
	}
		
}