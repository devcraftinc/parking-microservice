package example;

public class PricingPolicy {

	private static final int FIRST_INTERVAL_RATE = 10;
	private static final int INTERVAL_RATE = 3;

	public int calcPrice(long entryTime, long paymentTime) {
		long timeInParking = paymentTime - entryTime;
		if (timeInParking < minAsMillis(60))
			return FIRST_INTERVAL_RATE;
		
		long additionalTime = timeInParking - minAsMillis(60);
		return FIRST_INTERVAL_RATE + INTERVAL_RATE * numOfIntervals(additionalTime, 15);
	}

	private int numOfIntervals(long remainingTime, int intervalInMin) {
		int numOfIntervals = (int) (remainingTime/minAsMillis(intervalInMin)) + 1;
		return numOfIntervals;
	}

	private long minAsMillis(int min) {
		return min * 60 * 1000L;
	}

}