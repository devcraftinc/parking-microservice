package example;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingRepository extends CrudRepository<ParkingRepository.Parking, Long> {
	@Entity(name = "parking")
	public static class Parking {
		
		@GeneratedValue
		@Id
		public Long code;
		
		public long time;
	}
}