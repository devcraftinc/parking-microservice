package example;

public interface ParkingStore {

	long fetchEntryTime(long codeOnCard);

	long storeParkingEntry(long time);

}