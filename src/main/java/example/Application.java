package example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import example.ParkingPaymentService.Clock;

@RestController
@SpringBootApplication
public class Application extends SpringApplication {

	private static final class SystemClock implements Clock {
		@Override
		public long now() {
			return System.currentTimeMillis();
		}
	}

	@Autowired
	private ParkingPaymentService paymentService;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	Clock clock() {
		return new SystemClock();
	}
	
	@Bean
	ParkingPaymentService ParkingPaymentService(Clock clock, ParkingRepository repository) {
		return new ParkingPaymentService(clock,new JpaParkingStore(repository));
	}

	@PostMapping("/parking")
	public long enterParking() {
		return paymentService.enterParking();
	}

	@GetMapping("/parking/{code}/amount")
	public long calcPayment(@PathVariable long code) {
		return paymentService.calcPayment(code);
	}
}
