package example.cucumber;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import example.ClockMock;

public class TestApi {

	private MockMvc mvc;
	private ClockMock clock;

	public TestApi(MockMvc mvc, ClockMock clock) {
		this.mvc = mvc;
		this.clock = clock;
	}

	void setSystemTime(long time) {
		clock.setCurrTime(time);
	}

	long enterParking() throws Exception {
		MvcResult result = mvc.perform(post("/parking").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
		return Long.parseLong(result.getResponse().getContentAsString());
	}

	int calcPayment(long codeOnCard) throws Exception {
		MvcResult result = mvc.perform(get("/parking/" + codeOnCard + "/amount").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
		return Integer.parseInt(result.getResponse().getContentAsString());
	}
	
}