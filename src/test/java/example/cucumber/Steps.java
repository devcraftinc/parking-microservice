package example.cucumber;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.hamcrest.CoreMatchers;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps {
	
	TestApi testApi ;
	
	private long codeOnCard;
	private int actualAmount;

	public Steps(TestApi testApi) {
		this.testApi = testApi;
	}

	@Given("I entered the parking at {string}")
	public void i_entered_the_parking_at(String timeStr) throws Exception {
		testApi.setSystemTime(parseTime(timeStr));
		codeOnCard = testApi.enterParking(); 
	}

	@When("I pay at {string}")
	public void i_pay_at(String timeStr) throws Exception {
		testApi.setSystemTime(parseTime(timeStr));
		actualAmount = testApi.calcPayment(codeOnCard);
	}

	@Then("I should pay {int}")
	public void i_should_pay(int expectedAmount) {
		assertThat(actualAmount, is(expectedAmount));
	}
	
	private long parseTime(String timeStr) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
		return format.parse(timeStr).getTime();
	}
}