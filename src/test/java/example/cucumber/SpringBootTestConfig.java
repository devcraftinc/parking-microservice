package example.cucumber;

import java.text.ParseException;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;

import cucumber.api.java.en.Given;
import example.ClockMock;
import example.ParkingPaymentService.Clock;

/**
 * @author Eran
 * 
 * This is the tricky part.
 * cucumber-spring will find any steps class with @SpringBootTest and use it as a SpringBootTest.
 * We must have some "never-invoked" step for cucumber to understand this is a steps class.
 */
@SpringBootTest
@AutoConfigureMockMvc
public class SpringBootTestConfig {

	@Given("never invoked")
	public void neverInvoked() throws ParseException {
	}

	/**
	 * Override application beans and define test beans here.
	 */
	@TestConfiguration
	public static class TestConfig {
		
		@Bean
		Clock clock() {
			return new ClockMock();
		}
		
		@Bean
		TestApi testApi(MockMvc mvc, ClockMock clock) {
			return new TestApi(mvc, clock);
		}
	}
}