package example.cucumber;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import example.PricingPolicy;

public class PricingPolicyTest {

	private PricingPolicy pricingPolicy = new PricingPolicy();

	@Test
	public void shouldPayFirtHourRateOnFirstHour() throws Exception {
		assertThat(pricingPolicy.calcPrice(0L, minAsMillis(60) - 1), is(10));		
	}

	@Test
	public void shouldPayFirtIntervalOnFirstHourMark() throws Exception {
		assertThat(pricingPolicy.calcPrice(0L, minAsMillis(60)), is(13));		
	}

	@Test
	public void shouldPaySecondIntervalOnFirstIntervalMark() throws Exception {
		assertThat(pricingPolicy.calcPrice(0L, minAsMillis(75)), is(16));		
	}

	private long minAsMillis(int min) {
		return min * 60 * 1000L;
	}
}
