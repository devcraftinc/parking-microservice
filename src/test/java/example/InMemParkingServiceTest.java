package example;

public class InMemParkingServiceTest extends ParkingServiceTest {

	protected static class FakeParkingStore implements ParkingStore {

		@Override
		public long fetchEntryTime(long codeOnCard) {
			return codeOnCard;
		}

		@Override
		public long storeParkingEntry(long time) {
			return time;
		}
	}
	
	protected void setSystemTime(long time) {
		clockMock.setCurrTime(time);
	}

	private ClockMock clockMock = new ClockMock();
	private FakeParkingStore parkingStore = new FakeParkingStore();

	protected ParkingPaymentService makeParkingService() {
		return new ParkingPaymentService(clockMock, parkingStore);
	}

}
