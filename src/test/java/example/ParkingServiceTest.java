package example;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public abstract class ParkingServiceTest {

	ParkingPaymentService paymentService;

	@Before
	public void setup() {
		paymentService = makeParkingService();
	}

	abstract protected ParkingPaymentService makeParkingService();

	@Test
	public void shouldPay10OnFirstHour() {
		long code = enterParkingAt(0);
		long amount = calcPaymentAt(code, 60 * 60 * 1000L - 1);
		assertThat(amount, is(10L));
	}

	private long calcPaymentAt(long code, long time) {
		setSystemTime(time);
		long amount = paymentService.calcPayment(code);
		return amount;
	}

	private long enterParkingAt(int time) {
		setSystemTime(time);
		long code = paymentService.enterParking();
		return code;
	}

	abstract protected void setSystemTime(long time);

}