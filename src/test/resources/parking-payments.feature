Feature: parking payment

  Scenario: under a hour
    Given I entered the parking at "10:00:00"
    When I pay at "10:59:59"
    Then I should pay 10

  Scenario Outline: hourly parking
    Given I entered the parking at <entry_time>
    When I pay at <payment_time>
    Then I should pay <amount>

    Examples: 
      | entry_time | payment_time | amount |
      | "10:00:00" | "10:59:59"   |     10 |
      | "10:00:00" | "11:00:00"   |     13 |
      | "10:00:00" | "11:14:59"   |     13 |
      | "10:00:00" | "11:15:00"   |     16 |
